package utils

import (
	"time"
)

type LogStorage struct {
	Messages         []LogMessage
	MessageBus       chan LogMessage
	HearerMessageBus chan HearerMessage
	HearerMessages   []HearerMessage
	Users            []string
	UsersBus         chan []string
}

var instance *LogStorage = nil

func NewLogStorage() *LogStorage {
	if instance == nil {
		instance = new(LogStorage)
		instance.Messages = make([]LogMessage, 0)
		instance.MessageBus = make(chan LogMessage, 1024)
		instance.HearerMessages = make([]HearerMessage, 0)
		instance.HearerMessageBus = make(chan HearerMessage, 1024)
		instance.Users = make([]string, 0)
		instance.UsersBus = make(chan []string, 1024)
	}

	return instance
}

func (p *LogStorage) AddMessage(nick, text string, isHearer bool) {
	if len(p.Messages) == 20 {
		p.Messages = p.Messages[1:]
	}
	now := time.Now()
	msg := LogMessage{now, now.Format("15:04:05"), nick, text, isHearer}
	p.Messages = append(p.Messages, msg)
	p.MessageBus <- msg
}

func (p *LogStorage) AddHearerMessage(nick, text, IP string) {
	if len(p.HearerMessages) == 20 {
		p.HearerMessages = p.HearerMessages[1:]
	}
	msg := HearerMessage{time.Now(), nick, text, IP}
	p.HearerMessages = append(p.HearerMessages, msg)
	p.HearerMessageBus <- msg
	p.AddMessage(nick, text, true)
}

func (p *LogStorage) ChangeUsers(users []string) {
	p.Users = users
	p.UsersBus <- p.Users
}
