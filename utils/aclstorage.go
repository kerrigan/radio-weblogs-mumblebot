package utils

type ACLStorage struct {
	bannedUsers []string
	bannedIPs   []string
}

var aclStorageInstance *ACLStorage = nil

func NewACLStorage() *ACLStorage {
	if aclStorageInstance == nil {
		aclStorageInstance = new(ACLStorage)
		aclStorageInstance.bannedUsers = make([]string, 0)
		aclStorageInstance.bannedIPs = make([]string, 0)
	}

	return aclStorageInstance
}

func (p *ACLStorage) BanByUser(username string) {
	for _, v := range p.bannedUsers {
		if v == username {
			return
		}
	}

	p.bannedUsers = append(p.bannedUsers, username)
}

func (p *ACLStorage) BanByIP(IP string) {
	for _, v := range p.bannedIPs {
		if v == IP {
			return
		}
	}

	p.bannedIPs = append(p.bannedIPs, IP)
}

func (p *ACLStorage) UnbanUser(username string) {
	i := -1

	for index, v := range p.bannedUsers {
		if v == username {
			i = index
			break
		}
	}

	if i != -1 {
		p.bannedUsers = append(p.bannedUsers[:i], p.bannedUsers[i+1:]...)
	}
}

func (p *ACLStorage) UnbanIP(IP string) {
	i := -1

	for index, v := range p.bannedIPs {
		if v == IP {
			i = index
			break
		}
	}

	if i != -1 {
		p.bannedIPs = append(p.bannedIPs[:i], p.bannedIPs[i+1:]...)
	}
}

func (p *ACLStorage) IsBanned(username, IP string) bool {
	for _, v := range p.bannedIPs {
		if v == IP {
			return true
		}
	}

	for _, v := range p.bannedUsers {
		if v == username {
			return true
		}
	}

	return false
}

func (p ACLStorage) GetBannedUsers() []string {
	return p.bannedUsers
}

func (p ACLStorage) GetBannedIPs() []string {
	return p.bannedIPs
}
