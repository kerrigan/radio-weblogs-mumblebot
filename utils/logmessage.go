package utils

import (
	"time"
)

type LogMessage struct {
	Timestamp time.Time
	Date      string
	Nick      string
	Text      string
	IsHearer  bool
}

type HearerMessage struct {
	Timestamp time.Time
	Nick      string
	Text      string
	IP        string
}
