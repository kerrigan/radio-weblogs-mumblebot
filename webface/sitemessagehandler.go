package site

import (
	"fmt"
	"html"
	"log"
	"sort"
	"strings"
	"sync"

	mumbot "bitbucket.org/kerrigan/gomumbot/mumbot"
	utils "bitbucket.org/kerrigan/radio-weblogs-mumblebot/utils"
)

type SiteMessageHandler struct {
	logStorage  *utils.LogStorage
	bot         *mumbot.MumbleBot
	ctrlChannel chan int
	lock        sync.Mutex
}

func NewSiteMessageHandler(bot *mumbot.MumbleBot) SiteMessageHandler {
	h := SiteMessageHandler{}
	h.bot = bot
	h.logStorage = utils.NewLogStorage()
	h.ctrlChannel = make(chan int)
	h.lock = sync.Mutex{}

	go h.processIncoming()
	return h
}

func (p *SiteMessageHandler) ProcessMessage(bot *mumbot.MumbleBot, user *mumbot.User, private bool, message string) {
	if !private {
		p.logStorage.AddMessage(user.Nick, message, false)
	} else {
		aclStorage = utils.NewACLStorage()

		switch {
		case message == ".stats":
			go p.getIcecastsStats(bot, user)
		case strings.HasPrefix(message, ".ban "):
			args := strings.SplitN(message, " ", 2)
			if len(args) == 1 {
				bot.WritePrivateMessage(user.ID, "Usage: .ban USERID")
				return
			}

			siteUserId := args[1]

			aclStorage.BanByUser(siteUserId)
			log.Println(siteUserId, "now banned")
		case strings.HasPrefix(message, ".unban "):
			args := strings.SplitN(message, " ", 2)
			if len(args) == 1 {
				bot.WritePrivateMessage(user.ID, "Usage: .ban USERID")
				return
			}

			siteUserId := strings.TrimSpace(args[1])
			aclStorage.UnbanUser(siteUserId)
			log.Println(siteUserId, "now unbanned")
		case strings.HasPrefix(message, ".banip "):
			args := strings.SplitN(message, " ", 2)
			if len(args) == 1 {
				bot.WritePrivateMessage(user.ID, "Usage: .banip IP")
				return
			}

			IP := strings.TrimSpace(args[1])
			aclStorage.BanByIP(IP)
			log.Println(IP, "now banned")
		case strings.HasPrefix(message, ".unbanip "):
			args := strings.SplitN(message, " ", 2)
			if len(args) == 1 {
				bot.WritePrivateMessage(user.ID, "Usage: .unbanip IP")
				return
			}

			IP := args[1]

			aclStorage.UnbanIP(IP)
			log.Println(IP, "now unbanned")

		case strings.HasPrefix(message, ".banned"):
			bot.WritePrivateMessage(user.ID,
				fmt.Sprintf("Users:<br>%s<br>IPs:<br>%s",
					strings.Join(aclStorage.GetBannedUsers(), "<br>"),
					strings.Join(aclStorage.GetBannedIPs(), "<br>")))
		}
	}
}

func (p *SiteMessageHandler) UserStateChanged(b *mumbot.MumbleBot, users map[uint32]*mumbot.User) {
	usersnicks := make([]string, 0)
	botUserID := b.GetUserID()

	for uid, user := range users {
		if uid != botUserID && user.ChannelID == users[botUserID].ChannelID {
			usersnicks = append(usersnicks, user.Nick)
		}
	}

	sort.Strings(usersnicks)
	p.logStorage.ChangeUsers(usersnicks)
}

func (p SiteMessageHandler) processIncoming() {
	for {
		var message utils.HearerMessage
		select {
		case message = <-p.logStorage.HearerMessageBus:
		case <-p.ctrlChannel:
			return
		}

		text := html.EscapeString(message.Text)

		p.bot.WriteMessage(fmt.Sprintf("<font color='red'>%s@%s> %s</font>", message.Nick, message.IP, text))
	}
}

func (p *SiteMessageHandler) Stop() {
	p.ctrlChannel <- 1
}
