package site

import (
	"fmt"
	"html"
	"log"
	"math/rand"
	"net"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	utils "bitbucket.org/kerrigan/radio-weblogs-mumblebot/utils"
	"github.com/dchest/captcha"
	"github.com/flosch/pongo2"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

var logStorage = utils.NewLogStorage()
var aclStorage = utils.NewACLStorage()
var connections map[net.Addr]*websocket.Conn = make(map[net.Addr]*websocket.Conn)

var chatTmpl = pongo2.Must(pongo2.FromFile("./templates/chat.html"))
var feedbackTmpl = pongo2.Must(pongo2.FromFile("./templates/feedback.html"))

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func Run(httpPort int) {

	r := mux.NewRouter()

	r.Handle("/captcha/{id}.png", captcha.Server(captcha.StdWidth, captcha.StdHeight))
	r.HandleFunc("/feedback", FeedBackHandler)

	r.HandleFunc("/", HomeHandler)
	r.HandleFunc("/ws", WsHandler)

	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static/"))))

	go writeLoop(logStorage.MessageBus, logStorage.UsersBus)

	l, err := net.Listen("tcp4", fmt.Sprintf(":%v", httpPort))
	if err != nil {
		log.Fatal(err)
	}
	if err := http.Serve(l, r); err != nil {
		log.Fatal(err)
	}
}

func broadcast(message interface{}) {
	for _, conn := range connections {
		conn.WriteJSON(message)
	}
}

func writeLoop(msgChan chan utils.LogMessage, usersChan chan []string) {
	for {
		msg := WSPayload{}
		select {
		case message := <-msgChan:
			msg.Type = "message"
			if message.IsHearer {
				message.Nick = "слушатель"
				message.Text = html.EscapeString(message.Text)
			}
			msg.Payload = message
		case users := <-usersChan:
			msg.Type = "users"
			msg.Payload = users
		}

		broadcast(msg)
	}
}

func readLoop(conn *websocket.Conn) {
	for {
		if _, _, err := conn.NextReader(); err != nil {
			conn.Close()
			delete(connections, conn.RemoteAddr())
			//fmt.Println(connections)
			break
		}
	}
}

type WSPayload struct {
	Type    string      `json:"type"`
	Payload interface{} `json:"data"`
}

func WsHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)

	if err != nil {
		log.Println(err)
		return
	}
	connections[conn.RemoteAddr()] = conn
	//fmt.Println(connections)
	go readLoop(conn)
}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	err := chatTmpl.ExecuteWriter(pongo2.Context{
		"messages": logStorage.Messages,
		"users":    logStorage.Users,
	}, w)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func FeedBackHandler(w http.ResponseWriter, r *http.Request) {
	captchaError := ""
	userNameCookie, err := r.Cookie("user")

	if err != nil {
		userNameCookie = &http.Cookie{Name: "user", Value: randomNick()}
		http.SetCookie(w, userNameCookie)
	}

	if matched, _ := regexp.MatchString("[a-z]{6}[0-9]{1,2}",
		userNameCookie.Value); !matched {
		userNameCookie = &http.Cookie{Name: "user", Value: randomNick()}
		http.SetCookie(w, userNameCookie)
	}

	userName := userNameCookie.Value
	realIP := getRealIP(r)

	if checkBanned(userName, realIP) {
		http.Error(w, "U R BANNED", http.StatusForbidden)
		return
	}

	if r.Method == "POST" {

		captchaString := r.FormValue("captcha")
		captchaId := r.FormValue("captchaId")
		message := r.FormValue("message")

		if !captcha.VerifyString(captchaId, captchaString) {
			captchaError = `<p class="text-danger">Неправильная капча</p>`
		} else if len(message) > 200 {
			captchaError = `<p class="text-danger">Сообщение длиннее 200 символов</p>`
		} else {
			//fmt.Println(message)
			logStorage.AddHearerMessage(userName, message, realIP)
		}

		if len(captchaError) > 0 {
			captchaError = `<div class="input-group-addon">` + captchaError + `</div>`
		}
	}

	d := captcha.New()

	err = feedbackTmpl.ExecuteWriter(pongo2.Context{
		"captcha": d,
		"error":   captchaError,
	}, w)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func getRealIP(r *http.Request) string {
	if ip, ok := r.Header["X-Forwarded-For"]; ok {
		return ip[0]
	} else {
		return strings.Split(r.RemoteAddr, ":")[0]
	}
}

func checkBanned(userName, IP string) bool {
	return aclStorage.IsBanned(userName, IP)
}

func randomNick() string {
	letters := []rune("abcdefghijklmnopqrstuvwxyz")
	b := make([]rune, 6)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	result := string(b)

	result += strconv.Itoa(rand.Intn(100))

	return result
}
