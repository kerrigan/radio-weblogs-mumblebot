package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	mumbot "bitbucket.org/kerrigan/gomumbot/mumbot"
	site "bitbucket.org/kerrigan/radio-weblogs-mumblebot/webface"
)

func main() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)

	var botNick = flag.String("nickname", "gomumbot", "ник бота")
	var ip = flag.String("ip", "localhost", "ip-адрес сервера mumble")
	var port = flag.Int("port", 64738, "порт сервера mumble")
	var webPort = flag.Int("webport", 8081, "порт веб-сайта")
	flag.Parse()

	go site.Run(*webPort)

	bot := mumbot.NewMumbleBot(*ip, *port, *botNick)

	mh := site.NewSiteMessageHandler(bot)

	bot.SetMessageHandler(&mh)
	bot.SetUserStateHandler(&mh)

	go func() {
		for _ = range c {
			mh.Stop()
			bot.Stop()
			fmt.Println("Stopping mumble bot.")
			os.Exit(0)
		}
	}()

	bot.Run()
}
